; If you are using Nightlight, turn on the "InstaREPL" toggle
; so you can see the evaluated results on the left

; PART 1: Scalar values

; In Clojure, we define variables with `def`
(def score 1)

; We can add one like this
(+ score 1)

; But, notice that the original variable is unmodified
score

; We can fix that by redefining the variable
(def score (+ score 1))

; And now it retains the new value
score

; Variables can also hold strings
(def player-name "Alice")

; Let's get the first two letters
(subs player-name 0 2)

; But remember, you must redefine the variable to keep that value
(def player-name (subs player-name 0 2))

player-name

; There is also a special type in Clojure called a keyword
(def occupation :farmer)

; Think of them as lightweight strings, easy to type and read
occupation

; PART 2: Compound values

; Let's hold several names in a single variable using a vector
(def names ["Alice" "Bob" "Charlie"])

; You can add a name with `conj`
(conj names "David")

; But, once again, the original value isn't modified by default
names

; So, let's redefine the variable
(def names (conj names "David"))

names

; You can get a value from a vector by index
(get names 0)

; Or get the total number of values
(count names)

; This function randomizes the order
(shuffle names)

; And this one gets a single random value
(rand-nth names)

; We can also make an associative variable called a hash map
(def player {:name "Alice" :age 25})

; Hash maps associate keys with values
; The keys are keywords, which is common in Clojure
player

; With hash maps, you get a value by key instead of index
(get player :name)

; If the key is a keyword, you can use this shorthand
(:name player)

; You can use `conj` on hash maps too
(conj player [:score 5])

; But, it's more common to use `assoc`
(assoc player :score 5)

; Let's add it to the hash map
(def player (assoc player :score 5))

player

; PART 3: Functions and control flow

; Let's make a function that prepends something on a string
(defn add-greeting [name]
  (str "Hello, " name))

; Now let's try it out
(add-greeting player-name)

; Now let's make a function that decides if you won
(defn check-score [num]
  (if (> num 0)
    "You won!"
    "You lost!"))

(check-score score)

; How about a function to check if someone can drink alcohol?
(defn can-drink? [person]
  (>= (:age person) 21))

(can-drink? player)

; In the past, we made variables with `def`
; But that is only useful if you want to make a global variable
; If you only want the variables to be in a single function, use `let`
(defn create-message [person]
  (let [greeting (add-greeting (:name person))
        result (check-score (:score person))]
    (str greeting ". " result)))

(create-message player)

; Clojure rocks!
