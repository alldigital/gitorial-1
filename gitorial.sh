#!/bin/sh

cd `dirname $0`

case "$(uname -s)" in
    'Darwin') executable='.gitorial/darwin' ;;
    'Linux') executable='.gitorial/linux' ;;
    'FreeBSD') executable='.gitorial/freebsd' ;;
    *       ) echo "unknown os ($(uname -s))"; exit; ;;
esac

$executable
