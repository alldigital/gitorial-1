(ns basics.core)

(def table (.getElementById js/document "people"))
(def rows (.-rows table))
(def header-row (.-cells (.item rows 0)))
(def body-rows (aget (.-tBodies table) 0))
(def body-rows-array (.from js/Array (.-rows body-rows)))

(doseq [i (range (.-length header-row))]
  (.addEventListener (.item header-row i) "click"
    (fn []
      (.sort body-rows-array
        (fn [a b]
          (compare
            (.-textContent (aget (.-cells a) i))
            (.-textContent (aget (.-cells b) i)))))
      (doseq [row (array-seq body-rows-array)]
        (.appendChild body-rows row)))))
