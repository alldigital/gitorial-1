(set-env!
  :source-paths #{"src"}
  :resource-paths #{"resources"}
  :dependencies '[[nightlight "1.7.2"]
                  [org.clojure/clojure "1.8.0"]
                  [ring "1.5.1"]
                  [hiccup "1.0.5"]
                  [adzerk/boot-cljs "1.7.228-2" :scope "test"]
                  [org.clojure/clojurescript "1.9.854" :scope "test"]])

(require
  '[adzerk.boot-cljs :refer [cljs]]
  '[nightlight.boot :refer [nightlight]]
  'basics.core)

(deftask run []
  (comp
    (wait)
    (cljs :source-map true :optimizations :none)
    (nightlight :port 4000)
    (with-pass-thru _
      (basics.core/-main))))

(deftask build []
  (comp
    (cljs :optimizations :advanced)
    (aot :namespace #{'basics.core})
    (uber)
    (jar :main 'basics.core)
    (target)))

